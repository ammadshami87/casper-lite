#include "Factorial.h"

unsigned long long Factorial::GetFactorial(int n)
{
    /*
    unsigned long long factorial = 1;
    for(int i = 1; i <= input; ++i)
    {
        factorial *= i;
    }

    return factorial;
    */

    if(n > 1)
        return n * GetFactorial(n - 1);
    else
        return 1;
}
